Role Name
=========

Ansible role to install analytics zoo python module

Requirements
------------

pip should be preinstalled on the system

Role Variables
--------------


Dependencies
------------

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
